//
//  StartViewController.swift
//  High Performance
//
//  Created by Alex Balan on 01/08/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    private var timer:Timer!

    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimer), userInfo: nil, repeats: true)
    }

    @objc func runTimer(){
        performSegue(withIdentifier: "toMainScreen", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
