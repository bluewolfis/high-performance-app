//
//  CheckCircle.swift
//  High Performance
//
//  Created by Alex Balan on 29/05/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class CheckCircle: UIImageView {

    var checked:Bool = false
    var delegate:DaysSettingsController?

    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSublayers(of layer: CALayer) {

        let tap = UITapGestureRecognizer(target: self, action: #selector(onChecked(sender:)))

        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }


    @objc func onChecked(sender:UITapGestureRecognizer){

        if(isChecked()){
            unCheckIt()
        }else{
            checkIt()
        }

        print("checked",checked)

        if delegate != nil {
            delegate?.checkColors()
        }
    }

    func checkIt(){
        checked = true
        self.image = #imageLiteral(resourceName: "circlefull")
    }

    func unCheckIt(){
        checked = false
        self.image = #imageLiteral(resourceName: "circleempty")
    }

    func setup(check:Bool){
        unCheckIt()
        if check {
            checkIt()
        }
    }

    func isChecked () -> Bool{
        return checked
    }
}
