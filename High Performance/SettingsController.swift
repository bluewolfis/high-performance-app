//
//  SettingsController.swift
//  High Performance
//
//  Created by Alex Balan on 16/07/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {

    @IBOutlet weak var startTimePicker: UIDatePicker!
    @IBOutlet weak var endTimePicker: UIDatePicker!

    @IBOutlet weak var deactivateCheck: UISwitch!
    @IBOutlet weak var deactivateAllCheck: UISwitch!


    override func viewDidLoad() {
        super.viewDidLoad()

        startTimePicker.setValue(UIColor.white, forKey: "textColor")
        endTimePicker.setValue(UIColor.white, forKey: "textColor")

        getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getData(){
        let defaults = UserDefaults.standard

        if defaults.bool(forKey: "deactivateForToday"){
            if let dt = defaults.object(forKey: "today") as? Date {
                let today = Date()

                if Calendar.current.compare(today, to: dt, toGranularity: .year) == ComparisonResult.orderedSame {
                    if Calendar.current.compare(today, to: dt, toGranularity: .month) == ComparisonResult.orderedSame {
                        if Calendar.current.compare(today, to: dt, toGranularity: .day) == ComparisonResult.orderedSame {

                            deactivateCheck.isOn = true

                        }
                    }
                }

            }
        }

        if defaults.bool(forKey: "deactivateAll"){
            deactivateAllCheck.isOn = true
        }

        if let st = defaults.object(forKey: "startTime") {
            startTimePicker.date = st as! Date
        }

        if let et = defaults.object(forKey: "endTime") {
            endTimePicker.date = et as! Date
        }
    }

    func setData(){

        if startTimePicker.date > endTimePicker.date {
        //    endTimePicker.date = startTimePicker.date
        }

        let defaults = UserDefaults.standard

        defaults.set(startTimePicker.date, forKey: "startTime")
        defaults.set(endTimePicker.date, forKey: "endTime")
        defaults.set(deactivateCheck.isOn, forKey: "deactivateForToday")

        defaults.set(deactivateAllCheck.isOn, forKey: "deactivateAll")

        defaults.set(Date(), forKey: "today")
    }


    @IBAction func onBackTxt(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onStartTime(_ sender: Any) {
        setData()
    }

    @IBAction func onEndTime(_ sender: Any) {
        setData()
    }
    
    @IBAction func onDeactivate(_ sender: Any) {
        setData()
    }
    @IBAction func onDeactivateAll(_ sender: Any) {
        setData()
    }

}
