//
//  DaysSettingsController.swift
//  High Performance
//
//  Created by Alex Balan on 16/07/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class DaysSettingsController: UIViewController {

    @IBOutlet weak var emonday: CheckCircle!
    @IBOutlet weak var etuesday: CheckCircle!
    @IBOutlet weak var ewednesday: CheckCircle!
    @IBOutlet weak var ethursday: CheckCircle!
    @IBOutlet weak var efriday: CheckCircle!
    @IBOutlet weak var esaturday: CheckCircle!
    @IBOutlet weak var esunday: CheckCircle!

    @IBOutlet weak var emondayLbl: UILabel!
    @IBOutlet weak var emondayBtn: UIButton!

    @IBOutlet weak var etuesdayLbl: UILabel!
    @IBOutlet weak var etuesdayBtn: UIButton!

    @IBOutlet weak var ewednesdayLbl: UILabel!
    @IBOutlet weak var ewednesdayBtn: UIButton!

    @IBOutlet weak var ethursdayLbl: UILabel!
    @IBOutlet weak var ethursdayBtn: UIButton!

    @IBOutlet weak var efridayLbl: UILabel!
    @IBOutlet weak var efridayBtn: UIButton!

    @IBOutlet weak var esaturdayLbl: UILabel!
    @IBOutlet weak var esaturdayBtn: UIButton!

    @IBOutlet weak var esundayLbl: UILabel!
    @IBOutlet weak var esundayBtn: UIButton!

    @IBOutlet weak var allWeekBtn1: UIButton!
    @IBOutlet weak var allWeekBtn2: UIButton!
    @IBOutlet weak var allWeekBtnMask: UIButton!



    override func viewDidLoad() {
        super.viewDidLoad()

        emonday.delegate = self
        etuesday.delegate = self
        ewednesday.delegate = self
        ethursday.delegate = self
        efriday.delegate = self
        esaturday.delegate = self
        esunday.delegate = self

        getStoredData()

    }

    override func viewWillAppear(_ animated: Bool) {
       // roundCorners(btn: allWeekBtn1, corners: .bottomRight)
       // roundCorners(btn: allWeekBtn2, corners: [.bottomRight, .topRight])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getStoredData() {
        let defaults = UserDefaults.standard

        if defaults.bool(forKey: "setupused") {

            Defaults.monday = defaults.bool(forKey: "emonday")
            Defaults.tuesday = defaults.bool(forKey: "etuesday")
            Defaults.wednesday = defaults.bool(forKey: "ewednesday")
            Defaults.thursday = defaults.bool(forKey: "ethursday")
            Defaults.friday = defaults.bool(forKey: "efriday")
            Defaults.saturday = defaults.bool(forKey: "esaturday")
            Defaults.sunday = defaults.bool(forKey: "esunday")

            emonday.setup(check: Defaults.monday!)
            etuesday.setup(check: Defaults.tuesday!)
            ewednesday.setup(check: Defaults.wednesday!)
            ethursday.setup(check: Defaults.thursday!)
            efriday.setup(check: Defaults.friday!)
            esaturday.setup(check: Defaults.saturday!)
            esunday.setup(check: Defaults.sunday!)
            
        }else{
            emonday.setup(check: true)
            etuesday.setup(check: true)
            ewednesday.setup(check: true)
            ethursday.setup(check: true)
            efriday.setup(check: true)
            esaturday.setup(check: true)
            esunday.setup(check: true)

            setData()
        }

        checkColors()



    }

    func checkColors(){

        allWeekBtn1.setTitleColor(UIColor.purple, for: .normal)

        allWeekBtn1.backgroundColor = UIColor.white
        allWeekBtn2.backgroundColor = UIColor.white

        allWeekBtn1.layer.borderColor = UIColor.white.cgColor
        allWeekBtn1.layer.borderWidth = 1

        allWeekBtn2.layer.borderColor = UIColor.white.cgColor
        allWeekBtn2.layer.borderWidth = 1

        allWeekBtnMask.alpha = 0.0

        esundayLbl.textColor = UIColor.purple
        esundayBtn.backgroundColor = UIColor.white
        addShaddow(btn: esundayBtn)

        if esunday.checked {
            esundayLbl.textColor = UIColor.white
            esundayBtn.backgroundColor = UIColor.purple
            esundayBtn.layer.borderColor = UIColor.green.cgColor

        }

        esaturdayLbl.textColor = UIColor.purple
        esaturdayBtn.backgroundColor = UIColor.white
        addShaddow(btn: esaturdayBtn)

        if esaturday.checked {
            esaturdayLbl.textColor = UIColor.white
            esaturdayBtn.backgroundColor = UIColor.purple
            esaturdayBtn.layer.borderColor = UIColor.green.cgColor
        }

        efridayLbl.textColor = UIColor.purple
        efridayBtn.backgroundColor = UIColor.white
        addShaddow(btn: efridayBtn)

        if efriday.checked {
            efridayLbl.textColor = UIColor.white
            efridayBtn.backgroundColor = UIColor.purple
            efridayBtn.layer.borderColor = UIColor.green.cgColor
        }

        ethursdayLbl.textColor = UIColor.purple
        ethursdayBtn.backgroundColor = UIColor.white
        addShaddow(btn: ethursdayBtn)

        if ethursday.checked {
            ethursdayLbl.textColor = UIColor.white
            ethursdayBtn.backgroundColor = UIColor.purple
            ethursdayBtn.layer.borderColor = UIColor.green.cgColor
        }

        ewednesdayLbl.textColor = UIColor.purple
        ewednesdayBtn.backgroundColor = UIColor.white
        addShaddow(btn: ewednesdayBtn)

        if ewednesday.checked {
            ewednesdayLbl.textColor = UIColor.white
            ewednesdayBtn.backgroundColor = UIColor.purple
            ewednesdayBtn.layer.borderColor = UIColor.green.cgColor

        }

        etuesdayLbl.textColor = UIColor.purple
        etuesdayBtn.backgroundColor = UIColor.white
        addShaddow(btn: etuesdayBtn)

        if etuesday.checked {
            etuesdayLbl.textColor = UIColor.white
            etuesdayBtn.backgroundColor = UIColor.purple
            etuesdayBtn.layer.borderColor = UIColor.green.cgColor

        }

        emondayLbl.textColor = UIColor.purple
        emondayBtn.backgroundColor = UIColor.white
        addShaddow(btn: emondayBtn)

        if emonday.checked {
            emondayLbl.textColor = UIColor.white
            emondayBtn.backgroundColor = UIColor.purple
            emondayBtn.layer.borderColor = UIColor.green.cgColor

        }

        if emonday.checked && etuesday.checked && ewednesday.checked && ethursday.checked && efriday.checked {
            allWeekBtn1.backgroundColor = UIColor.purple
            allWeekBtn2.backgroundColor = UIColor.purple

            allWeekBtn1.layer.borderColor = UIColor.green.cgColor
            allWeekBtn2.layer.borderColor = UIColor.green.cgColor

            allWeekBtnMask.alpha = 1.0

            allWeekBtn1.setTitleColor(UIColor.white, for: .normal)
        }

    }

    func addShaddow(btn:UIButton){
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 10
    }

    func setData(){
        let defaults = UserDefaults.standard

        defaults.set(emonday.checked, forKey: "emonday")
        defaults.set(etuesday.checked, forKey: "etuesday")
        defaults.set(ewednesday.checked, forKey: "ewednesday")
        defaults.set(ethursday.checked, forKey: "ethursday")
        defaults.set(efriday.checked, forKey: "efriday")
        defaults.set(esaturday.checked, forKey: "esaturday")
        defaults.set(esunday.checked, forKey: "esunday")

        defaults.set(true , forKey: "setupused")
    }

    func checkAllWeekdays(){


        if emonday.checked && etuesday.checked && ewednesday.checked && ethursday.checked && efriday.checked {
            emonday.unCheckIt()
            etuesday.unCheckIt()
            ewednesday.unCheckIt()
            ethursday.unCheckIt()
            efriday.unCheckIt()
        }else{
            emonday.checkIt()
            etuesday.checkIt()
            ewednesday.checkIt()
            ethursday.checkIt()
            efriday.checkIt()
        }

        checkColors()
    }


    @IBAction func onDone(_ sender: Any) {

        setData()

        dismiss(animated: true, completion: nil)
    }

    @IBAction func onMonday(_ sender: Any) {
        emonday.onChecked(sender: UITapGestureRecognizer())
        checkColors()

    }
    @IBAction func onTuesday(_ sender: Any) {
        etuesday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }
    @IBAction func onWednesday(_ sender: Any) {
        ewednesday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }
    @IBAction func onThursday(_ sender: Any) {
        ethursday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }
    @IBAction func onFriday(_ sender: Any) {
        efriday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }
    @IBAction func onSaturday(_ sender: Any) {
        esaturday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }

    @IBAction func onSunday(_ sender: Any) {
        esunday.onChecked(sender: UITapGestureRecognizer())
        checkColors()
    }

    @IBAction func allWeek1(_ sender: Any) {
        checkAllWeekdays()
    }
    @IBAction func allWeek2(_ sender: Any) {
        checkAllWeekdays()
    }


}


func roundCorners(btn:UIButton, corners:UIRectCorner) {
    btn.clipsToBounds = true
        let radius = 10.0
        let path = UIBezierPath(roundedRect: btn.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = btn.bounds
        mask.path = path.cgPath
        btn.layer.mask = mask

}

