//
//  ViewController.swift
//  High Performance
//
//  Created by Alex Balan on 29/05/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit
import CoreMotion
import UserNotifications

class ViewController: UIViewController {

    private var debug:Bool = false

    private var timer:Timer!

    private var stepsCount:String = "0"
    private var activityType:String = ""

    private var ticktime = 0

    private let activityManager = CMMotionActivityManager()
    private let pedometer = CMPedometer()

    private var thumbshow = false
    
    @IBOutlet weak var moveCircle: CheckCircle!
    @IBOutlet weak var waterCircle: CheckCircle!
    @IBOutlet weak var thinkCircle: CheckCircle!

    @IBOutlet weak var activityTypeLabel: UILabel!
    @IBOutlet weak var stepsCountLabel: UILabel!

    @IBOutlet weak var debugView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        getStoredData()

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimer), userInfo: nil, repeats: true)

        startUpdating()

        setupNotification()

        print("did load")
        debugView.isHidden = true
        if debug {
            debugView.isHidden = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        print("will appear")

        if ticktime > 10 {
            getStoredData()
            setupNotification()
        }
    }

    func getStoredData() {
        let defaults = UserDefaults.standard

        Defaults.monday = defaults.bool(forKey: "emonday")
        Defaults.tuesday = defaults.bool(forKey: "etuesday")
        Defaults.wednesday = defaults.bool(forKey: "ewednesday")
        Defaults.thursday = defaults.bool(forKey: "ethursday")
        Defaults.friday = defaults.bool(forKey: "efriday")
        Defaults.saturday = defaults.bool(forKey: "esaturday")
        Defaults.sunday = defaults.bool(forKey: "esunday")

        Defaults.deactivateForToday = defaults.bool(forKey: "deactivateForToday")
        Defaults.deactivateAll = defaults.bool(forKey: "deactivateAll")

        if Defaults.deactivateForToday! {

            if let dt = defaults.object(forKey: "today") as? Date {

                Defaults.today = dt

                let today = Date()

                Defaults.deactivateForToday = false

                if Calendar.current.compare(today, to: dt, toGranularity: .year) == ComparisonResult.orderedSame {
                    if Calendar.current.compare(today, to: dt, toGranularity: .month) == ComparisonResult.orderedSame {
                        if Calendar.current.compare(today, to: dt, toGranularity: .day) == ComparisonResult.orderedSame {

                            Defaults.deactivateForToday = true

                        }
                    }
                }

            }
        }

        if let st = defaults.object(forKey: "startTime") {
            Defaults.startTime = (st as! Date)
        }else{

            Defaults.startTime = Calendar.current.date(bySettingHour: 10, minute: 0, second: 0, of: Date())
        }

        if let et = defaults.object(forKey: "endTime") {
            Defaults.endTime = (et as! Date)
        }else{

            Defaults.endTime = Calendar.current.date(bySettingHour: 22, minute: 0, second: 0, of: Date())
        }

        //print("start \(Defaults.startTime)")
        //print("end \(Defaults.endTime)")
    }

    @objc func runTimer(){
        //print("tick \(ticktime)")
        activityTypeLabel.text = activityType
        stepsCountLabel.text = stepsCount

        ticktime = ticktime + 1

        if(!thumbshow){

            if waterCircle.isChecked() && thinkCircle.isChecked() && moveCircle.isChecked() {

                thumbshow = true

                performSegue(withIdentifier: "showThumb", sender: self)

            }

        }

    }

    private func startTrackingActivityType() {
        activityManager.startActivityUpdates(to: OperationQueue.main) {
            [weak self] (activity: CMMotionActivity?) in

            guard let activity = activity else { return }

                if activity.walking {
                    self?.activityType = "Walking"
                } else if activity.stationary {
                    self?.activityType = "Stationary"
                } else if activity.running {
                    self?.activityType = "Running"
                } else if activity.automotive {
                    self?.activityType = "Automotive"
                } else if activity.cycling {
                    self?.activityType = "Cycling"
                }

            print("ping")
        }
    }

    private func startCountingSteps() {
        pedometer.startUpdates(from: Date()) {
            [weak self] pedometerData, error in
            guard let pedometerData = pedometerData, error == nil else { return }

            self?.stepsCount = pedometerData.numberOfSteps.stringValue
        }
    }

    private func startUpdating() {
        if CMMotionActivityManager.isActivityAvailable() {
            startTrackingActivityType()
        }

        if CMPedometer.isStepCountingAvailable() {
            startCountingSteps()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onMoveBtn(_ sender: Any) {
        moveCircle.onChecked(sender: UITapGestureRecognizer())
    }

    @IBAction func onWaterBtn(_ sender: Any) {
        waterCircle.onChecked(sender: UITapGestureRecognizer())
    }

    @IBAction func onThinkBtn(_ sender: Any) {
        thinkCircle.onChecked(sender: UITapGestureRecognizer())
    }

    @IBAction func unwind(segue:UIStoryboardSegue) {
        uncheck()
        thumbshow = false
    }

}

extension ViewController:UNUserNotificationCenterDelegate{

    func setupNotification(){
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound];

        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                print("notifications not allowed")

                center.requestAuthorization(options: options) {
                    (granted, error) in
                    if !granted {
                        print("Something went wrong")
                    }else{
                        self.handleNotification(center: center)
                    }
                }

            }else{
                self.handleNotification(center: center)
            }
        }
    }

    func handleNotification(center:UNUserNotificationCenter){

        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()

        let content = UNMutableNotificationContent()
        content.title = "Don't forget to"
        content.body = "Drink some water, Start moving for 5 minutes. \nAsk yourself if this is the best use of your time."
        content.sound = UNNotificationSound.default()

        center.delegate = self

        var interval = Int((Defaults.endTime?.timeIntervalSince(Defaults.startTime!))! / 3600)

        if(interval < 0){
            Defaults.endTime = Defaults.endTime?.addingTimeInterval(3600 * 24)
            interval = Int((Defaults.endTime?.timeIntervalSince(Defaults.startTime!))! / 3600)
        }
        print("interval \(interval)")

        Defaults.startTime = Calendar.current.date(bySettingHour: Calendar.current.component(.hour, from: Defaults.startTime!), minute: Calendar.current.component(.minute, from: Defaults.startTime!), second: 0, of: Date())

        let stDateTime = Defaults.startTime

        var usedDateTime = stDateTime

        for d in 0...3 { // days

            var dayComponent = DateComponents()
            dayComponent.day = d

            for i in 0...interval { // hours

                dayComponent.hour = i

                usedDateTime = Calendar.current.date(byAdding: dayComponent, to: stDateTime!)

                if(usedDateTime! > Date()){

                    var isok = false

                    if Defaults.monday! && Calendar.current.component(.weekday, from: usedDateTime!) == 2 {
                        isok = true
                    }
                    if Defaults.tuesday! && Calendar.current.component(.weekday, from: usedDateTime!) == 3 {
                        isok = true
                    }
                    if Defaults.wednesday! && Calendar.current.component(.weekday, from: usedDateTime!) == 4 {
                        isok = true
                    }
                    if Defaults.thursday! && Calendar.current.component(.weekday, from: usedDateTime!) == 5 {
                        isok = true
                    }
                    if Defaults.friday! && Calendar.current.component(.weekday, from: usedDateTime!) == 6 {
                        isok = true
                    }
                    if Defaults.saturday! && Calendar.current.component(.weekday, from: usedDateTime!) == 7 {
                        isok = true
                    }
                    if Defaults.sunday! && Calendar.current.component(.weekday, from: usedDateTime!) == 1 {
                        isok = true
                    }


                    if d == 0 && Defaults.deactivateForToday! {
                        isok = false
                    }

                    if Defaults.deactivateAll! {
                        isok = false
                    }

                    if isok {

                        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: usedDateTime!), repeats: false)

                        let identifier = UUID().uuidString + " Reminders "
                        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                        center.add(request, withCompletionHandler: { (error) in
                            if let error = error {
                                print("error: \(error)")
                            }else{
                                print("handle notification ok \(identifier)")
                            }
                        })
                    }
                }

            }
        }

        // to verify

        center.getPendingNotificationRequests { (requests) in
            for request in requests {
                print(request.trigger.debugDescription)
            }
        }

    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("notification received, app open from background")
        completionHandler()

        uncheck()

    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("notification received while app in foreground")
        //completionHandler([.alert,.sound])

        uncheck()
    }

    func uncheck(){
        // uncheck
        moveCircle.unCheckIt()
        thinkCircle.unCheckIt()
        waterCircle.unCheckIt()
    }

}

