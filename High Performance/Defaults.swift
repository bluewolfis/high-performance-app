//
//  Defaults.swift
//  High Performance
//
//  Created by Alex Balan on 16/07/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import Foundation

struct Defaults {

    static var startTime:Date?
    static var endTime:Date?

    static var deactivateForToday:Bool?
    static var deactivateAll:Bool?
    
    static var today:Date?

    static var monday:Bool?
    static var tuesday:Bool?
    static var wednesday:Bool?
    static var thursday:Bool?
    static var friday:Bool?
    static var saturday:Bool?
    static var sunday:Bool?
}
