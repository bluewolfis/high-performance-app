//
//  ThrumbUpViewController.swift
//  High Performance
//
//  Created by Alex Balan on 02/08/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class ThrumbUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.performSegue(withIdentifier: "unwind", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
